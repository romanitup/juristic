Juristic
========

*Juristic* is a Ruby implementation of contracts. It allows you to:

  * specify what values methods should accept and return
  * specify values variables can hold
  * create general purpose contracts for any part of your system and integrate it
  in a manner you see fit.

A few important features:

  * Specifying what a value can or cannot be is NOT limited by a class/type of the value,
  but is completely flexible - you can write arbitrary ruby code to validate values.

  * All checks are done only in runtime, so it doesn't quite work like Sorbet or other
  static type checkers would.

  * Juristic statements are written inside your program's code: inside a method when
  you want to validate what a method can accept or not, or by a variable you declare
  when you want to validate a variable. Apart from runtime checks, this also allows
  for self documenting code.


Basic usage
-----------

**Validating method input (what it can accept)**

      # Array notation
      def say_hello1(greeting1, greeting2, print_to=STDOUT)
        accepts [:greeting1, greeting1, String, Array, -> (v) { v.length > 0 } ],
                [:greeting2, greeting2, String, "length > 0; must not be empty"],
                [:print_to,  print_to,  :or, Logger, IO                        ]

        # ... method body
      end

      # Same thing in block notation
      def say_hello2(greeting1, greeting2, print_to=STDOUT)
        accepts do |c|
          c.and :greeting1, greeting1,
            String, Array, -> (v) { v.length > 0 }
          c.and :greeting2, greeting2,
            String, "length > 0; must not be empty"
          c.or :print_to, print_to, Logger, IO

        # ... method body
        end
      end

**Validating methods return values**

The same syntax can be applied when testing return values, except that you'll also
need to wrap the return value in the special `#r` method, which, effectively, calls the
validation. That's because returns from methods can happen in different places,
but it makes sense to define conditions for the return value once:

    def say_hello1(greeting)
      # note the 's' at the end of `returns` -  this method isn't the same as `return` keyword.
      # `returns()` method call defines conditions for a return value, and `r()` later on
      # actually validates the value against these conditions.
      returns String, "length > 3"

      if greeting == "hello"
        r("#{greeting} world!")
      else
        r(greeting)
      end
    end

    say_hello("hi there!") # -> "hi there!"
    say_hello("hello")     # -> "hello world!"
    say_hello("hi")        # -> ERROR: Juristic::InvalidContract on return attempt


**Validating variable values**

To validate that a variable only ever holds a value that satisfies your conditions,
you use a special method `#JVar`, passing variable value as the first argument and
conditions as consecutive arguments:

    v = JVar "hello", JC.kind_of(String), JC.longer_than(3)

    # or the same thing easier:
    v = JVar "hello", String, JC.longer_than(3)
    v.class # -> String

    # let's trigger the validation:
    v.sub!("hello", "hi") # -> Juristic::InvalidContract error is raised.

This code will validate the variable whenever ANY method is called on the object (in addition to the moment immediately after initialization). That might be a problem in most cases,
so one can specify which methods trigger the validation:

    v = JVar "hello", String, JC.longer_than(3), validate_on: :sub!

    # now let's try to trigger an error by calling the regullar, non-modyfing :sub method:
    v.sub("hello", "hi") # -> all good, no errors are raised!

    # but if we call the modifyer :sub! method changing variable value
    # to an invalid on, an error will be raised:
    v.sub!("hello", "hi") # -> Juristic::InvalidContract error is raised.

How it works under the hood
---------------------------
`#accepts`, `#returns` and `#JVar` methods actually create special objects of class
`Juristic::Contract`, which contain the conditions and the logic that validates the
values (passed later on, possibly multiple times) against these conditions.
`Juristic::Contract` objects don't raise any errors if validations fail - instead,
when you call `Juristic::Contract#apply_to` method, they either return `nil`
(when there are no validation errors, or, as we'll be calling them - [contract] `violations`)
or an array of violation messages. Let's look at how it works:

    c = Juristic::Contract.new
    c.add_conditions(
      -> (v) { v.kind_of?(String)  },
      -> (v) { !v.empty?           }
    )

    c.apply_to("hello") # -> nil
    c.apply_to("") # ->
      # [:and, "value `` (String)\ndoesn't satisfy the condition
      # (details not specified).\nSee the condition here: path/to/file:line"]

In the case of an empty string, we get an array, which contains a not terribly
helpful error message, but also the symbol `:and` - this symbol describes the type
of contract. There are two types of contracts that you can create: a `:and` contract
and a `:or` contract. It's pretty easy to guess what's the difference between them:
in `:and` contracts, all conditions must be satisfied not to violate it, whereas in
`:or` contracts only one of the conditions must be satisfied. By default, all
contracts are `:and`, but let's see if we can get away without violations
if we change the contract above into an `:or` contract:

    c = Juristic::Contract.new(:or)
    c.add_conditions(
      -> (v) { v.kind_of?(String)  },
      -> (v) { !v.empty?           }
    )

    c.apply_to("hello") # -> nil, because value is String and not empty
    c.apply_to("")      # -> nil, because value is String (that's enough!)
    c.apply_to([1])     # -> nil, because value is NOT empty (that's enough!)

Why yes we can! Finally, let's maybe add a more useful error message:

    c = Juristic::Contract.new(:and)
    c.add_conditions(
      [ -> (v) { v.kind_of?(String) }, -> (v, v_alias) { "#{v} is not a String" }],
      [ -> (v) { !v.empty?          }, -> (v, v_alias) { "#{v} is empty"        }]
    )

    c.apply_to(1) # -> [:and, "1 is not a String"]

Here, we changed how we defined the conditions: each condition is an array of two
lambdas. The first lambda is the condition, while the second one generates
an error message (which includes both the actual value via the `v` variable and
the alias for this value via the `v_alias` - alias being useful when you create
contracts for method arguments, although this is done via a different syntax
described above).


Condition templates
-------------------

We can simplify our contract conditions description by using predefined
condition templates that come with this library:

    c = Juristic::Contract.new(:and)
    c.add_conditions(JC.kind_of(String), JC.longer_than(3), JC::shorter_than(10))

    c.apply_to("hello")        # -> nil
    c.apply_to("hello world!") # -> [:and, "value `hello world!`" +
                               #            must be shorter than 10, but it is not."]

Here's a full list of condition templates available:

  * `JС.kind_of(*classes)`
  * `JС.respond_do(*methods)`
  * `JС.more_than([Numeric])`
  * `JС.less_than([Numeric])`
  * `JС.longer_than([Numeric])`
  * `JС.shorter_than([Numeric])`
  * `JС.array_of(*classes)` - checks whether each element of array is an instance of one of the listed classes
  * `JС.hash_of(*key_classes => *value_classes)` - checks whether each key and each value is an instance of one of the listed classes


Using :and, :or and condition templates for methods and variables
-----------------------------------------------------------------

You don't need to explicitly create contract objects to make use of the features
described above. Instead, you can just specify the type of contract and also
use condition templates with `#accepts`, `#returns` and `#JVar` methods:

    accepts [:greeting1, greeting1, :and, String, JC.longer_than(0) ]
    returns :or, JC.longer_than(0), JC.shorter_than(10)
    JVar "hello", :or, JC.longer_than(0), JC.shorter_than(10)

Contracts as conditions for contracts
-------------------------------------

You create a contract and pass it to the `Juristic::Contract#add_conditions` of
another contract effectively making it a subcontract. That way, you can create
complex contract structures that basically resemble using `()` (brackets) in logical
expressions: that is, you can have `:or` contract inside an `:and` contract or vice versa.
