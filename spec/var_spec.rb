require_relative '../lib/juristic'

describe Juristic::Var do

  it "creates a wrapper around an object and validates it against the contract immediately upon initialization" do
    v = JVar "hello", JC.kind_of(String), JC.longer_than(3)
    expect( -> { v.sub!("hello", "hi") }).to raise_error(Juristic::InvalidContract)
    expect( -> { JVar [], JC.kind_of(String), JC.longer_than(3) })
      .to raise_error(Juristic::InvalidContract)
  end

  it "validates the wrapped object against the contract only when a method from the supplied list is called on the object" do
    v = JVar "hello", JC.kind_of(String), JC.longer_than(3), validate_on: :sub!
    v.gsub!("hello", "hi")
    expect( -> { v.sub!("hi", "hey") }).to raise_error(Juristic::InvalidContract)
  end

  it "exposes the class object of the wrapped object with the #class method" do
    v = JVar "hello"
    expect(v.class).to eq(String)
  end

  it "allows a simplified syntax for creating contracts checking if value is of a certain class" do
    JVar "hello", String, Array
    JVar ["hello"], String, Array
    expect( -> { JVar 1, String, Array }).to raise_error(Juristic::InvalidContract)
  end

end
