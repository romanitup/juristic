require_relative '../lib/juristic'

class Dummy

  def say_hello1(greeting1, greeting2, print_to=STDOUT)
    accepts do |c|
      c.and :greeting1, greeting1,
        String, -> (v) { v.length > 0 }
      c.and :greeting2, greeting2,
        String, "length > 0" # <- this string will eval to `greeting2.length > 0`
      c.or :print_to, print_to, Array, IO
    end

    returns String, "length > 0"
    r(greeting1)
  end

  def say_hello2(greeting1, greeting2, print_to=STDOUT)
    accepts [:greeting1, greeting1, String, Array, -> (v) { v.length > 0 } ],
            [:greeting2, greeting2, String, "length > 0; must not be empty"],
            [:print_to,  print_to,  :or, Array, IO                         ]


    returns :or, String, -> (v) { v.length > 0 }
    r(greeting1)
  end

end

describe Juristic::ObjectExtension do

  before(:each) do
    @dummy = Dummy.new
  end

  describe "specifying contract conditions for the arguments accepted by a method" do

    it "uses block notation" do
      @dummy.say_hello1("hi", "hello", STDOUT)
      expect(-> { @dummy.say_hello1("", 1, false)}).to raise_error(Juristic::InvalidContract)
    end

    it "uses array notation" do
      @dummy.say_hello2("hi", "hello", STDOUT)
      expect(-> { @dummy.say_hello2("", 1, false)}).to raise_error(Juristic::InvalidContract)
    end

  end

  describe "specifying contract conditions for the return value" do

    it "checks conditions with AND contract" do
      expect(@dummy.say_hello1("hello", "hi")).to eq("hello")
      expect( -> { @dummy.say_hello1("", "hello") }).to raise_error(Juristic::InvalidContract)
    end

    it "checks conditions with OR contract" do
      expect(@dummy.say_hello2([1], "hi")).to eq([1])
      expect( -> { @dummy.say_hello1([], "hello") }).to raise_error(Juristic::InvalidContract)
    end

  end

end
