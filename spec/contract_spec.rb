require_relative '../lib/juristic'

describe Juristic::Contract do

  before(:each) do
    @top_c = Juristic::Contract.new(:and)
  end

  it "creates a nested condition set" do
    c1 = Juristic::Contract.new(:and)
    c2 = Juristic::Contract.new(:and)
    c1.add_conditions(
      -> (v) { v.kind_of?(String)  },
      -> (v) { !v.empty?           }
    )
    c2.add_conditions(
      -> (v) { v.kind_of?(Integer) },
      -> (v) { v > 0               },
      -> (v) { v < 10              }
    )
    @top_c.add_conditions(c1)
    @top_c.add_conditions(c2)
    expect(@top_c.conditions.length).to eq(2)
    expect(@top_c.conditions[0].length).to eq(2)
    expect(@top_c.conditions[1].length).to eq(3)
  end

  it "applies a single level condition set to a value" do
    @top_c.add_conditions(
      -> (v) { v.kind_of?(String)  },
      -> (v) { !v.empty?           }
    )
    expect(@top_c.apply_to("hello")).to be_nil
  end

  it "allows value aliases to be passed so that we know which value contract violation are related to" do
    @top_c.add_conditions(-> (v) { v.length < 10 })
    expect(@top_c.apply_to("hello world!", :greeting)[1]).to include("`greeting` value `hello world!` (String)\ndoesn't satisfy the condition")
  end

  it "applies a multilevel condition set to a value" do
    @top_c.logical_operation = :or
    c1 = Juristic::Contract.new(:and)
    c2 = Juristic::Contract.new(:and)
    c1.add_conditions(
      -> (v) { v.kind_of?(String)  },
      -> (v) { !v.empty?           }
    )
    c2.add_conditions(
      -> (v) { v.kind_of?(Integer) },
      -> (v) { v > 0               },
      -> (v) { v < 10              }
    )
    @top_c.add_conditions(c1, c2)

    expect(@top_c.apply_to(1)).to be_nil
    expect(@top_c.apply_to(11).slice(1..-1).length).to eq(2)
    expect(@top_c.apply_to(0).slice(1..-1).length).to eq(2)
    expect(@top_c.apply_to("hello")).to be_nil
    expect(@top_c.apply_to("").slice(1..-1).length).to eq(2)
  end

  it "filters a passed value before applying conditions to it" do
    cs = Juristic::Contract.new { |v| v[0] }
    cs.add_conditions(-> (v) { v > 0 })
    expect(cs.apply_to([10])).to be_nil
    expect(cs.apply_to([0]).slice(1..-1).length).to eq(1)
  end

  describe "condition templates" do

    it "checks if value has class or module somewhere in its inheritance tree" do
      @top_c.add_conditions Juristic::CT.kind_of(String, File)
      expect(@top_c.apply_to(0)[1]).to eq("value `0` must be an object of one of the following classes:" +
                                          "\n  String, File\nbut it is an object of class Integer.\n")
      expect(@top_c.apply_to("hello")).to be_nil
    end

    it "checks whether an object representing the value responds to the specified method calls" do
      @top_c.add_conditions Juristic::CT.respond_to(:length, :size)
      expect(@top_c.apply_to(0)[1]).to include("must respond to all of the following methods")
      expect(@top_c.apply_to("hello")).to be_nil
      expect(@top_c.apply_to(["some", "array"])).to be_nil
    end

    it "checks whether a number is more or less than another number" do
      @top_c.add_conditions Juristic::CT.more_than(3)
      expect(@top_c.apply_to(1)[1][1]).to eq("value `1` must be > `3`, but it is not.")
      expect(@top_c.apply_to(4)).to be_nil

      @top_c.conditions = []
      @top_c.add_conditions Juristic::CT.less_than(3)
      expect(@top_c.apply_to(4)[1][1]).to eq("value `4` must be < `3`, but it is not.")
      expect(@top_c.apply_to(2)).to be_nil
    end

    it "checks whether a string is longer than n characters" do
      @top_c.add_conditions Juristic::CT.longer_than(3)
      expect(@top_c.apply_to("hi")[1]).to eq("value `hi` must be longer than 3, but it is not.")
      expect(@top_c.apply_to("hello")).to be_nil

      @top_c.conditions = []
      @top_c.add_conditions Juristic::CT.shorter_than(3)
      expect(@top_c.apply_to("hello")[1]).to eq("value `hello` must be shorter than 3, but it is not.")
      expect(@top_c.apply_to("hi")).to be_nil
    end

    it "checks if array contains objects of specific classes only" do
      @top_c.add_conditions Juristic::CT.array_of(String, Integer)
      expect(@top_c.apply_to([nil, nil, 1, "hello"])[1]).to include("must contain items only of the following classes")
      expect(@top_c.apply_to(["hello", "world", 1])).to be_nil
    end

    it "checks if array contains objects of specific classes only" do
      @top_c.add_conditions Juristic::CT.hash_of([String] => [Integer])
      expect(@top_c.apply_to({ 1 => "hello" })[1]).to include("must contain keys only of the following classes")
      expect(@top_c.apply_to({ "hello" => 1})).to be_nil
    end

  end

end
