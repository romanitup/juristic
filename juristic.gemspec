Gem::Specification.new do |s|
  s.name           = "juristic"
  s.version        = "0.0.1"
  s.date           = "2020-08-05"
  s.summary        = "Code contracts implementation for Ruby"
  s.description    = "Code contracts implementation for Ruby"
  s.authors        = ["romanitup"]
  s.email          = "romanitup@protonmail.com"
  s.files          = ["lib/juristic"]
  s.license        = "MIT"
end
