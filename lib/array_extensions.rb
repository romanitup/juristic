class Array

  def self.wrap(obj)
    obj.kind_of?(Array) ? obj : [obj]
  end

end
