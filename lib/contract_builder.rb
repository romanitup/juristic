module Juristic
  class ContractBuilder

    class << self

      def create(conditions, violation_message: nil)
        #extract all class conditions into 1 condition, because checking
        # whether a value is kind_of? a certain class is by definition
        # an OR operation.
        contract_type = [:or, :and].include?(conditions.first) ? conditions.shift : :and
        class_conditions = conditions.select { |s| s.kind_of?(Class) }
        class_conditions += conditions.select do |s|
          s.kind_of?(Array) && s.select { |i| i.kind_of?(Class) }.size == s.size
        end
        other_conditions = conditions - class_conditions
        class_conditions.flatten!

        contract = Juristic::Contract.new(contract_type)
        contract.add_conditions(Juristic::CT.kind_of(*class_conditions)) unless class_conditions.empty?
        other_conditions.each do |condition|
          if [Proc, Contract, Array].include?(condition.class)
            contract.add_conditions condition
          else
            contract.add_conditions self.parse_condition(condition)
          end
        end
        contract
      end

      def parse_condition(c)
        if c.kind_of?(String)
          condition, violation_message = c.split(";")
          result = []
          result << -> (v) { eval("v.#{condition}") }
          if violation_message
            result <<
              -> (v, v_alias) { violation_message.strip + ", but it was `#{v}` (#{v.class})" }
          end
          result
        elsif c == :void
          # if we're even calling a apply_to on void contract, we violate it!
          [
            -> (v) { true },
            -> (v, v_alias) { "contract is void (should not be checked), this it was violated!" }
          ]
        else
          raise ParseError, "Cannot parse contract condition for the argument.\n"       +
                            "it must be either a Class constant,\n"                     +
                            "a String (that evals into ruby code), or a lambda/proc,\n" +
                            "but it is #{c.inspect} (#{c.class})."
        end

      end

    end

  end
  class ParseError < StandardError;end
end
