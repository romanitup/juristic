module Juristic
end

require_relative 'types'
require_relative 'string_extensions'
require_relative 'array_extensions'
require_relative 'condition'
require_relative 'contract'
require_relative 'invalid_contract'
require_relative 'condition_templates'
require_relative 'var'
require_relative 'object_extensions'
require_relative 'contract_builder'

J   = Juristic
JT  = Juristic::Types
JC  = Juristic::ConditionTemplates

define_method "JVar" do |*args|
  keyword_args = args.last.kind_of?(Hash) ? args.pop : {}
  Juristic::Var.new(*args, **keyword_args)
end
