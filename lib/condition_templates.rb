module Juristic
  class ConditionTemplates
    class << self

      def kind_of(*classes)
        [
          -> (v) {
            result = false
            classes.each { |c| result = true and break if v.kind_of?(c) }
            result
          },
          -> (v, v_alias) {
            <<~MSG
            #{v_alias.wrap_if("`").append_space_if}value `#{v}` must be an object of one of the following classes:
              #{classes.map(&:to_s).join(", ")}
            but it is an object of class #{v.class.to_s}.
            MSG
          }
        ]
      end

      def array_of(*classes)
        [
          -> (arr) { array_contains_objects_of_classes_only?(arr, classes) },
          -> (v, v_alias) {
            <<~MSG
            #{v_alias.wrap_if("`").append_space_if}array `#{v}` must contain items only of the following classes:
              #{classes.map(&:to_s).join(", ")}
            but it contained objects of other classes too.
            MSG
          }
        ]
      end

      def hash_of(hash_of_keys_and_value_classes)
        key_classes   = Array.wrap(hash_of_keys_and_value_classes.keys.first)
        value_classes = Array.wrap(hash_of_keys_and_value_classes.values.first)
        [
          -> (h) {
            if h.kind_of?(Hash)
              array_contains_objects_of_classes_only?(h.keys, key_classes) &&
              array_contains_objects_of_classes_only?(h.values, value_classes)
            else
              false
            end
          },
          -> (v, v_alias) {
            <<~MSG
            #{v_alias.wrap_if("`").append_space_if}array `#{v}` must contain keys only of the following classes:
              #{key_classes.map(&:to_s).join(", ")}
            and values only of the following classes:
              #{value_classes.map(&:to_s).join(", ")}
            but it contained objects of other classes too.
            MSG
          }
        ]
      end

      def respond_to(*methods)
        [
          -> (v) {
            result = true
            methods.each { |m| result = false and break unless v.respond_to?(m) }
            result
          },
          -> (v, v_alias) {
            <<~MSG
            #{v_alias.wrap_if("`").append_space_if}value `#{v}` must respond to all of the following methods:
              #{methods.map(&:to_s).join(", ")}
            but it does not.
            MSG
          }
        ]
      end

      def more_than(n)
        Contract.new(:and, [kind_of(Numeric), compare(n, :>)])
      end

      def less_than(n)
        Contract.new(:and, [kind_of(Numeric), compare(n, :<)])
      end

      def longer_than(n)
        Contract.new(:and, [respond_to(:length), -> (v) { v.length > n }], violation_message:
          -> (v, v_alias) { "#{v_alias.wrap_if("`").append_space_if}value `#{v}` must be longer than #{n}, but it is not." })

      end

      def shorter_than(n)
        Contract.new(:and, [respond_to(:length), -> (v) { v.length < n }],
        violation_message:
          -> (v, v_alias) { "#{v_alias.wrap_if("`").append_space_if}value `#{v}` must be shorter than #{n}, but it is not." })
      end

      private

      def compare(n, operation)
        [
          -> (v) { v.send(operation, n) },
          -> (v, v_alias) {
            "#{v_alias.wrap_if("`").append_space_if}value `#{v}` must be #{operation} `#{n}`, but it is not."
          }
        ]
      end

      def compare_length(n, operation)
        [
          -> (v) { v.length.send(operation, n) },
          -> (v, v_alias) {
            "#{v_alias.wrap_if("`").append_space_if}value `#{v}` length must be #{operation} `#{n}`, but it is not."
          }
        ]
      end

      def array_contains_objects_of_classes_only?(arr, classes)
        return false unless arr.kind_of?(Array)
        result = true
        arr.select do |i|
          if classes.select { |c| i.kind_of?(c) }.size == 0
            result = false
            break
          end
        end
        result
      end

    end
  end
  CT = ConditionTemplates
end
