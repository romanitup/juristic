module Juristic
  module ObjectExtension

    def accepts(*args_and_conditions, &block)
      if block_given?
        accepts_block_notation(&block)
      else
        accepts_array_notation(*args_and_conditions)
      end
    end

    def returns(*conditions)
      @return_contracts ||= {}
      unless @return_contracts[method_name_for_contract(caller)]
        @return_contracts[method_name_for_contract(caller)] =
          Juristic::ContractBuilder.create(conditions)
      end
    end

    def r(value)
      violations = @return_contracts[method_name_for_contract(caller)].apply_to(value)
      violations ? (raise InvalidContract.new(violations)) : value
    end


    private

    def accepts_array_notation(*args_and_conditions)
      args_and_conditions = [args_and_conditions] unless args_and_conditions.first.kind_of?(Array)
      violations = []

      args_and_conditions.each do |statement|
        arg_name  = statement.shift
        arg_value = statement.shift
        arg_contract = Juristic::ContractBuilder.create(statement)
        result = arg_contract.apply_to(arg_value)
        result&.shift

        if result
          violation_messages = result.join("\n#{arg_contract.logical_operation}DOUBLE_NEWLINE")
          violation_messages.gsub!("\n", "\n\t")
          violation_messages.gsub!("DOUBLE_NEWLINE", "\n\n")
          violations << "* #{arg_name} violations:\n"           +
                        "-----------------------------------\n" +
                        violation_messages
        end
      end
      raise InvalidContract.new(violations) unless violations.empty?
    end

    # Converts block notation into an array notation
    # and the calls accepts_array_notation
    def accepts_block_notation
      converter = BlockToArratNotationConverter.new
      yield(converter)
      accepts_array_notation *(converter.args_and_conditions_array)
    end

    def method_name_for_contract(method_caller_arr)
      method_caller_arr.first.match(/in `([^']+?)'/)[1]
    end

    class BlockToArratNotationConverter

      attr_reader :args_and_conditions_array

      def initialize
        @args_and_conditions_array = []
      end

      def and(*args)
        @args_and_conditions_array << args.insert(2, :and)
      end

      def or(*args)
        @args_and_conditions_array << args.insert(2, :or)
      end
    end # BlockNotationMethods class

  end # ObjectExtension module

end

Object.include Juristic::ObjectExtension
