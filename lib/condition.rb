module Juristic
  class Condition

    def initialize(condition, violation_message)
      @condition         = condition
      @violation_message = violation_message
    end

    def apply_to(v, v_alias=nil)
      v_alias = v_alias.to_s if v_alias.kind_of?(Symbol)
      unless @condition.call(v)
        if @violation_message
          @violation_message.call(v, v_alias)
        else
          "#{v_alias.wrap_if("`").append_space_if}value `#{v}` (#{v.class})\n" +
          "doesn't satisfy the condition (details not specified).\n"     +
          "See the condition here:\n  #{@condition.source_location.join(":")}"
        end
      end
    end

  end
end
