module Juristic
  class InvalidContract < StandardError

    def initialize(errors)
      errors.shift if [:or, :and].include?(errors.first)
      errors[0] = "\n" + errors[0]
      super(errors.join("\n\n"))
    end

  end
end
