module Juristic
  class Var < BasicObject

    attr_reader :obj

    def initialize(*conditions, violation_message: nil, raise_error: true, validate_on: nil)
      @obj          = conditions.shift
      @raise_error  = raise_error
      @validate_on  = ::Array.wrap(validate_on).map { |m| m.to_sym } if validate_on

      @contract = ContractBuilder.create(conditions, violation_message: violation_message)
      self.validate!
    end

    def validate!
      return if @validation_lock
      @validation_lock = true
      result = @contract.apply_to(@obj)
      raise InvalidContract.new(result) if result
      @validation_lock = false
      result
    end

    def class
      @obj.class
    end

    def method_missing(method_name, *args, &block)
      @obj.send(method_name, *args, &block)
      if @validate_on.nil? || @validate_on.include?(method_name)
        self.validate!
      end
    end

  end
end
