module Juristic
  class Contract

    attr_accessor :conditions, :logical_operation

    def initialize(logical_operation=:and, conditions=[], violation_message: nil, &value_filter)
      @value_filter      = value_filter
      @logical_operation = logical_operation
      @violation_message = violation_message
      self.add_conditions *conditions
    end

    def add_conditions(*conditions)
      @conditions ||= []
      conditions.map! do |c|
        unless c.kind_of?(Contract)
          c = [c] unless c.kind_of?(Array)
          c = Condition.new(c[0], c[1])
        end
        c
      end
      @conditions += conditions
    end

    def apply_to(v, v_alias=nil)
      v = @value_filter.call(v) if @value_filter
      violations = []
      @conditions.each do |c|
        result = c.apply_to(v, v_alias)
        violations << result unless result.nil? || result.empty?
        break if @logical_operation == :and && !result.nil?
      end
      return nil if @logical_operation == :or && @conditions.length > violations.length

      if violations.empty?
        nil
      else
        if @violation_message
          @violation_message.call(v, v_alias)
        else
          [@logical_operation] + violations
        end
      end
    end

    def length
      @conditions.length
    end

  end
end
