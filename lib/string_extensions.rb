class String

  def camelize
    self.split("_").collect do |s|
      s == s.upcase ? s : s.capitalize
    end.join
  end

  def underscore
    self.gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr("-", "_").
    downcase
  end

  # We also extend NilClass with the same method,
  # however nil class condition would always be false
  # and it basically returns an empty string.
  def concat_if(s2=" ", position=:after, &condition)
    condition ||= -> (s) { !s.empty? }
    if condition.call(self)
      position == :after ? self + s2 : s2 + self
    else
      s2
    end
  end

  def wrap_if(s2="", &condition)
    condition ||= -> (s) { !s.empty? }
    if condition.call(self)
      "#{s2}#{self}#{s2}"
    else
      self
    end
  end

  def append_space_if(&block)
    result = block ? block.call : !self.empty?
    result ? self + " " : self
  end

end

class NilClass
  def concat_if(s2=" ", position=:after, &condition);""end
  def wrap_if(s2=" ", &condition);""end
  def append_space_if; ""; end
end
